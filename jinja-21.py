# https://www.youtube.com/watch?v=bxhXQG1qJPM
# Conditionals
# Min 9:03

from jinja2 import Environment, FileSystemLoader

file_loader = FileSystemLoader('templates')
env = Environment(loader=file_loader)


template = env.get_template('truth.txt')
output = template.render(truth=True)
#output = template.render(truth=False)
print(output)
