import jinja2


{%- macro ip_Address_low(subnet) -%}
{% set subnet_split = subnet.split('/') %}
{% set ip_Add_low = subnet_split[0] %}
{{ ip_Add_low }}
{%- endmacro %}

{%- macro ip_Address_high(subnet) -%}
{% set sub = ip_Address_low(subnet) %}
{% set ip_Add = sub.split('.') %}
{% set four_Octet = (ip_Add[3]|int + 1)|string %}
{% set ip_Add_high = ip_Add[0] + "." + ip_Add[1] + "." + ip_Add[2] + "." + four_Octet %}
{{ ip_Add_high }}
{%- endmacro %}

{%- macro Subnet_CIDR(subnet) -%}
{% set subnet_split = subnet.split('/') %}
{% set cidr_Slash = subnet_split[1] %}
{{ cidr_Slash }}
{%- endmacro %}


{%- set v5_lower_subnet_one  = ip_Address_high(v5_Subnet_one) %}
{% set Subnet_Mask = Subnet_CIDR(v5_Subnet_one) %}
{%- set v7_lower_subnet_two =ip_Address_low(v7_Subnet_two)%}
{%- set v3_the_cidr_for_subnet_two =Subnet_CIDR(v7_Subnet_two)%}
{%- set v5_high_ip_subnet_two =ip_Address_high(v7_Subnet_two)%}




The value {{ main_value }} 
New line {{ main_value }} address {{ v5_lower_subnet_one }}/{{ Subnet_Mask }}
New data1 {{ v1_variable_one }} 
New data2 {{ v2_variable_two }} 
Lower IP subnet2 {{v7_lower_subnet_two}}
The cidr subnet2 is {{v3_the_cidr_for_subnet_two}}
High IP subnet2 {{v5_high_ip_subnet_two}}
