#http://zetcode.com/python/jinja/

#In the example, we define a Person object. We get the name and age via the two getters.

from jinja2 import Template  # This is a file that has clases defined: characteristicas and actions (render) 

#template that definie the characteristics (attributes) and actions (methods)
class Person:
#The person will have the characteritics name and age
#The person will have the actions getAge and getName

#Init create the clase ( the person)
#Self is the form to get access to the charactirics and person actions 
# 1) Create the person showing its characteristics ( name and age) 
    def __init__(self, name1, age1):

#the name entered as argument is assigned to the object to create 
        self.name = name1
        self.age = age1

#This is an action getAge
#The argument is self this give access to all  the characteristics already defined 
    def getAge(self):
        agex = self.age # Access to the characteristic age.  agex = entered age when person was created 
        return agex  

#This is an action getName
#The argument is self this give access to all  the characteristics already defined 
    def getName(self):
        return self.name    #access to name characteristic (same way that lines 23 to 25)


person = Person('Peter', 34)  # Here we are crating the person with
# the characteristic.name = 'Peter' and the characteristic.age = 34

print("Person created with name " , person.name)
print("Person create with age " , person.age)


tm = Template("My name is {{ personx.getName() }} and I am {{ personx.getAge() }}")
msg = tm.render(personx=person)
#tm is the template. is the file that has the varialbes 
#Line 33 are the values of variables  to be remplaced on the template 
#Line 41 is the function that merge variables in the template "tm"
#Ouput file

print(msg)