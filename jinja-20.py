from jinja2 import Template
template = Template('Hello {{ name }}!')
output = template.render(name='John Doe')
print(output)
