from jinja2 import Environment, FileSystemLoader

IP1 = "10.123.54.67"
IP2 = "100.123.54.88"
DeviceName = "Espana"




router = [
    {'IP1': IP1 , 'IP2': IP2, 'DeviceName': DeviceName },  
]

file_loader = FileSystemLoader('templates')
env = Environment(loader=file_loader)

template = env.get_template('routerTemplate.txt')

output = template.render(router=router)
print(output)
