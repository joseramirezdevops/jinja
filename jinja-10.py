# 1. Import variables
from jinja2 import Environment, FileSystemLoader

IP1data = '100.34.56.99'
IP2data = '10.34.56.10'
DeviceNamedata = 'Paris'


# 2. Create list with values to to replaced form incoming .tx file
routerValues = [ {'IP1': IP1data , 'IP2': IP2data , 'DeviceName': DeviceNamedata} ]


# 3. Use Jinja to replace variables values inside template .txt file
fileLoader= FileSystemLoader('templates') # Folder to read .txt file
env= Environment(loader=fileLoader) 

template= env.get_template('routerTemplate.txt') # file with the text template

output= template.render(router=routerValues) # this is the varialbe with the text already replaced
print(output)

# 4. Create a .txt file with the values from the template replaced and save it to hard disk
with open ('./outputs/replaced.txt', 'w') as filex:
    filex.write(output)