
#http://zetcode.com/python/jinja/
#We can use raw, endraw markers to escape Jinja delimiters.


from jinja2 import Template

data = "{{name}}"

tm = Template(data)
#tm = Template("{{name}}")

msg = tm.render(name='Peter')

print(msg)