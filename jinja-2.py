#http://zetcode.com/python/jinja/
#In the next example, we use two variables.


from jinja2 import Template  # This is a file that has clases defined: characteristicas and actions (render) 


name1 = 'Peter'
age1 = 34

tm = Template("My name is {{ name }} I am  {{ age }}")
# tm is an object with the characteristics and action defined in the file template on line 5
msg = tm.render(name=name1, age=age1) # Here the render action was execute that remplace values
#name1 and age1 of lines  7 and 8 into the varialbe name and age of line 12 

print(msg)
