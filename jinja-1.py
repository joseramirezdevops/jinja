
#http://zetcode.com/python/jinja/

#In the first example, we create a very simple template.
# python jinja-1.py

from jinja2 import Template  # This is a file that has clases defined: characteristicas and actions (render) 

name = input("Enter your name: ")

tm = Template("Hello {{ name }}")
msg = tm.render(name=name)

print(msg)
