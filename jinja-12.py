from jinja2 import Environment, FileSystemLoader

file_loader = FileSystemLoader('templates')
env = Environment(loader=file_loader)

color = input("Enter a color: ")
subnet = input("Enter the subnet ")

template = env.get_template('template-test.txt')

output = template.render(color=color, subnet=subnet)

 
print(output)
