
{%- macro ip_Address_low(subnet) -%}
{% set subnet_split = subnet.split('/') %}
{% set ip_Add_low = subnet_split[0] %}
{{ ip_Add_low }}
{%- endmacro %}

{%- macro ip_Address_high(subnet) -%}
{% set sub = ip_Address_low(subnet) %}
{% set ip_Add = sub.split('.') %}
{% set four_Octet = (ip_Add[3]|int + 1)|string %}
{% set ip_Add_high = ip_Add[0] + "." + ip_Add[1] + "." + ip_Add[2] + "." + four_Octet %}
{{ ip_Add_high }}
{%- endmacro %}



{%- macro Subnet_CIDR(subnet) -%}
{% set subnet_split = subnet.split('/') %}
{% set cidr_Slash = subnet_split[1] %}
{{ cidr_Slash }}
{%- endmacro %}



{%- set var3 = 3 %}
{%- set var4 = 4 + 5 %}
{%- set var5 = color %} 
{%- set var12 = subnet %} 

{%- set subnet  = ip_Address_low(subnet=subnet) %} 
{%- set subnetfirstIP  = ip_Address_high(subnet=subnet) %} 
{%- set CIDR = Subnet_CIDR(subnet='10.34.10.20/29') %}


{# https://jinja.palletsprojects.com/en/2.11.x/templates/#math #}

{% macro input(name, value='', type='text', size=20) -%}
    <input="{{ type }}" name="{{ name }}" value="{{
        value|e }}" size="{{ size }}">
{%- endmacro %}


{%- set var10= input('Leon', type='animal', size='10 kg', value = '$1000') %}


The color is  {{color}} 
addition {{ 1 + 1}}
{{var3}}
{{var4}}
{{var5}}
All macros are working!
{{CIDR}}
Subnet 
{{subnet}}
Subnet first IP 
{{subnetfirstIP}}
Web example Macro  is working
{{var10}}
Printing subnet entered 
{{var12}}