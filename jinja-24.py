# https://www.youtube.com/watch?v=bxhXQG1qJPM
#Child templates
# Min 13:20

from jinja2 import Environment, FileSystemLoader

file_loader = FileSystemLoader('templates')
env = Environment(loader=file_loader)

template = env.get_template('child.html')


output = template.render(title='Page Title', body='Stuff')
print(output)
