
#Jinja allows a convenient dot notation to access data in Python dictionaries.

from jinja2 import Template  # This is a file that has clases defined: characteristicas and actions (render) 


#line 7 is an equivalent to the database data
dictionary = { 'name': 'Person', 'age1': 34 }  # This is a dictionary 
# The label name points to value 'Person'
# The label age points to value 34


tm = Template("My name is {{ personObject.name }} and I am {{ personObject.age1 }}")
# personObjec.name  is charateristic name of object persone created . 
# .name on line 13 is related  to 'name' on line 8 
# .age1 on line 13 is related to 'age' on line 8 

  
# tm = Template("My name is {{ per['name'] }} and I am {{ per['age'] }}")
msg = tm.render(personObject=dictionary)
# personObject=dictionary 
print(msg)