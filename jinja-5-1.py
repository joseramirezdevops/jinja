
#http://zetcode.com/python/jinja/
#We can use raw, endraw markers to escape Jinja delimiters.

# Note: By using the raw, endraw block, we escape the Jinja {{ }} syntax. It is printed in its literal meaning.

from jinja2 import Template

data = '''
{% raw %}
His name is {{name}}
{% endraw %}
 '''

#This program prints the variable {{ name }}

tm = Template(data)
msg = tm.render(name= 'Peter')

print(msg)