# https://www.youtube.com/watch?v=bxhXQG1qJPM
#For loops 
# Min 10:40


from jinja2 import Environment, FileSystemLoader

file_loader = FileSystemLoader('templates')
env = Environment(loader=file_loader)

template = env.get_template('rainbow.txt')

colors = ['red' , 'green', 'blue']


output = template.render(colors=colors)
print(output)
