# https://www.youtube.com/watch?v=bxhXQG1qJPM
#Templete Inheritance
# Min 11:56

from jinja2 import Environment, FileSystemLoader

file_loader = FileSystemLoader('templates')
env = Environment(loader=file_loader)

template = env.get_template('base.html')


output = template.render(title='Page Title')
print(output)
